# VR Color Picker Demo

![Short video about the demo](DemoVideo.mp4)
![Another short video about the demo](DemoVideo2.mp4)

This is a color picker I made for a VR project. I decided to release the source
code for it in case anyone wants to take ideas from it or copy some code for
their own projects.

Some parts of the project that might be useful as examples or starting points
for your own code:
- Setting up a VR viewport (src/Main.gd)
- Using trackpads on VR controllers (src/Controllers/LeftController.gd and 
src/Controllers/RightController.gd)
- Point-and-click UI for VR (src/Controllers/Pointer.gd)
- Shaders using hue, sat, and val (src/Controllers/ColorPickerH.tres and
src/Controllers/ColorPickerSL.tres)

This project uses [godot-openvr](http://godotengine.org/asset-library/asset/150)
from the Asset Library.
