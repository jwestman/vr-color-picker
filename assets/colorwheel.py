#!/usr/bin/env python3

from math import sin, cos, radians

VERTICES = 72

def get_vertex(length, step):
    deg = step / VERTICES * 360
    return cos(radians(deg)) * length, sin(radians(deg)) * length

outer = [ get_vertex(1, i) for i in range(VERTICES) ]
inner = [ get_vertex(.75, i) for i in range(VERTICES) ]
hue = [ i / VERTICES for i in range(VERTICES + 1) ]

output = ""

for i in inner:
    output += "v {:.4f} {:.4f} 0\n".format(*i)

for i in outer:
    output += "v {:.4f} {:.4f} 0\n".format(*i)

for i in hue:
    output += "vt {:.4f} 0\n".format(i)

for i in range(VERTICES):
    inA = i + 1
    inB = ((i + 1) % VERTICES) + 1
    outA = inA + VERTICES
    outB = inB + VERTICES
    hueA = inA
    hueB = inA + 1

    output += "f {}/{} {}/{} {}/{}\n".format(
        inA,    hueA,
        inB,    hueB,
        outA,   hueA
    )
    output += "f {}/{} {}/{} {}/{}\n".format(
        inB,    hueB,
        outB,   hueB,
        outA,   hueA
    )

print(output)
