extends Area

const PaletteItem = preload("res://src/Controllers/PaletteItem.tscn")

const PALETTE_WIDTH = 4
const PALETTE_HEIGHT = 8

enum Mode { OFF=0, ON, DROPPER, ADD }

signal material_chosen()
signal mode_set(new_mode)

var current_material : PaletteMaterial
var hue := 0.0
var mode = Mode.OFF setget set_mode, get_mode

func _ready() -> void:
	_create_palette(true)
	_create_palette(false)

	_connect_rgb_sliders()

	set_current_material(PaletteMaterial.new(Color("808080")))

func set_current_material(mat: PaletteMaterial, set_hue:=true) -> void:
	current_material = mat
	mat.setup_spatial_material($CurrentColor/Mesh.material_override)
	if set_hue: self.hue = mat.color.h

	$Sliders/R.value = mat.color.r
	$Sliders/G.value = mat.color.g
	$Sliders/B.value = mat.color.b
	$Sliders/Label.text = "#" + mat.color.to_html().substr(2, 6)

	emit_signal("material_chosen", mat)

func set_color(color: Color) -> void:
	set_current_material(PaletteMaterial.new(color), color.h > 0)

func set_hue(hue: float) -> void:
	var color := current_material.color
	var new_color := Color.from_hsv(hue, color.s, color.v, color.a)
	set_current_material(PaletteMaterial.new(new_color))

func set_sat_val(sat: float, val: float) -> void:
	var color := current_material.color
	var new_color := Color.from_hsv(self.hue, sat, val, color.a)
	# do not set hue here
	set_current_material(PaletteMaterial.new(new_color), false)

func set_mode(new_mode: int) -> void:
	mode = new_mode
	emit_signal("mode_set", mode)

	$Sliders/Add/Button.is_toggled = new_mode == Mode.ADD

func get_mode() -> int:
	return mode

func _connect_rgb_sliders() -> void:
	$Sliders/R.connect("value_changed", self, "_on_slider_changed", ["R"])
	$Sliders/G.connect("value_changed", self, "_on_slider_changed", ["G"])
	$Sliders/B.connect("value_changed", self, "_on_slider_changed", ["B"])

func _on_slider_changed(value, slider) -> void:
	var c = current_material.color
	match slider:
		"R":
			set_color(Color(value, c.g, c.b))
		"G":
			set_color(Color(c.r, value, c.b))
		"B":
			set_color(Color(c.r, c.g, value))


func _create_palette(var local: bool) -> void:
	var palette : Spatial
	if local: palette = $Palettes/Local
	else: palette = $Palettes/Global

	for x in range(PALETTE_WIDTH):
		for y in range(PALETTE_HEIGHT):
			var item := PaletteItem.instance()
			item.id = x * PALETTE_HEIGHT + y
			item.translation.x = .05 * x + .025
			item.translation.z = .05 * y + .025
			item.local = local
			palette.add_child(item)

func pointer_enter(pointer) -> void:
	$AnimationPlayer.play("Hover")

func pointer_exit(pointer) -> void:
	$AnimationPlayer.play_backwards("Hover")
	if mode != Mode.ON:
		set_mode(Mode.OFF)


func _on_add_toggled() -> void:
	if $Sliders/Add/Button.is_toggled:
		set_mode(Mode.ADD)
	else:
		set_mode(Mode.OFF)
