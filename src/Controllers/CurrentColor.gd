extends Area

func click(pointer) -> void:
	var parent := get_parent()
	if parent.mode == parent.Mode.ON:
		parent.mode = parent.Mode.OFF
	else:
		parent.mode = parent.Mode.ON

func pointer_enter(pointer) -> void:
	$AnimationPlayer.play("Hover")

func pointer_exit(pointer) -> void:
	$AnimationPlayer.play_backwards("Hover")
