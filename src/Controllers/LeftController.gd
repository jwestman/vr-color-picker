extends ARVRController

func _ready() -> void:
	$ColorPicker.connect("material_chosen", self, "_on_material_chosen")
	$ColorPicker.connect("mode_set", self, "_on_color_picker_mode_set")
	_on_material_chosen($ColorPicker.current_material)

func _input(event: InputEvent) -> void:
	if event.device == get_joystick_id():
		if event is InputEventJoypadMotion:
			# omit garbage events when the "joystick"
			# returns to the center
			if event.axis_value == 0: return
			if not (event.axis in [0, 1]): return

			if $ColorPicker.mode:
				var pos := Vector2(get_joystick_axis(0), get_joystick_axis(1))
				if pos.length() > Utils.DEAD_ZONE:
					var hue := pos.angle_to(Vector2.UP) / deg2rad(360) + .5
					$ColorPicker.set_hue(hue)

func _on_material_chosen(mat: PaletteMaterial) -> void:
	var color := mat.get_spatial_material().albedo_color
	$Touchpad/ColorWheel.material_override.set_shader_param("current_color", color)
	$Touchpad/ColorWheel/Indicator/Mesh.material_override.albedo_color = color
	$Touchpad/ColorWheel/Indicator.rotation.x = Utils.hue_to_angle($ColorPicker.hue)

func _on_color_picker_mode_set(mode: int) -> void:
	if mode == $ColorPicker.Mode.ON:
		$Touchpad/ColorWheel.visible = true
	else:
		$Touchpad/ColorWheel.visible = false