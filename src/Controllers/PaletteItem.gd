extends Area

var id := 0
var local := false

var material : PaletteMaterial setget set_material

func _ready() -> void:
	if local:
		# todo: load the local color palette
		visible = false

	set_material(GlobalPalette.get_material(id))

func set_material(new_material: PaletteMaterial) -> void:
	material = new_material
	if material:
		material.setup_spatial_material($Mesh.material_override)
		$Mesh.visible = true
	else:
		$Mesh.visible = false


func pointer_enter(pointer) -> void:
	var picker = find_parent("ColorPicker")

	$AnimationPlayer.play("Hover")
	if picker.mode == picker.Mode.ADD:
		set_material(picker.current_material)

func pointer_exit(pointer) -> void:
	var picker = find_parent("ColorPicker")

	$AnimationPlayer.play_backwards("Hover")
	if picker.mode == picker.Mode.ADD:
		set_material(GlobalPalette.get_material(id))

func click(pointer) -> void:
	var picker = find_parent("ColorPicker")

	if picker.mode == picker.Mode.ADD:
		set_material(picker.current_material)
		GlobalPalette.set_material(id, picker.current_material)
		picker.mode = picker.Mode.OFF
	elif material:
		picker.set_current_material(material)
