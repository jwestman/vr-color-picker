extends Spatial

var active := false
var pointing_stack := []

var joystick_start := [null, null]

func _try_call(obj, function: String):
	if obj and obj.has_method(function):
		return obj.call(function, self)

# Gets the location of the pointer on the object
# being pointed to, in its local coordinates,
# or null
func get_pointer_location():
	var pointing_to = get_pointing_to()
	if not pointing_to: return null
	return pointing_to.to_local($RayCast.get_collision_point())

func click() -> void:
	if active:
		_try_call(get_pointing_to(), "click")

func click_hold() -> void:
	if active:
		_try_call(get_pointing_to(), "click_hold")

func joystick_motion(event: InputEventJoypadMotion) -> void:
	if not active: return

	if joystick_start[event.axis] == null:
		joystick_start[event.axis] = event.axis_value
	else:
		var delta = event.axis_value - joystick_start[event.axis]
		var step = _try_call(get_pointing_to(), "get_swipe_step")
		if step and abs(delta) > step:
			joystick_start[event.axis] = null
			get_parent().play_rumble_anim("Beep")
			if event.axis == 0:
				if delta < 0: _try_call(get_pointing_to(), "swipe_left")
				else: _try_call(get_pointing_to(), "swipe_right")
			elif event.axis == 1:
				if delta < 0: _try_call(get_pointing_to(), "swipe_up")
				else: _try_call(get_pointing_to(), "swipe_down")

func joystick_release() -> void:
	joystick_start = [null, null]

func get_pointing_to() -> Area:
	if pointing_stack.size():
		return pointing_stack.back()
	else:
		return null

func _process(delta: float) -> void:
	var pointing_to = $RayCast.get_collider()
	var old_pointing_to = get_pointing_to()
	if pointing_to != old_pointing_to:
		while true:
			if not old_pointing_to: break
			if old_pointing_to == pointing_to: break
			if pointing_to != null:
				if old_pointing_to.is_a_parent_of(pointing_to): break

			pointing_stack.pop_back()
			_try_call(old_pointing_to, "pointer_exit")
			old_pointing_to = get_pointing_to()

		if pointing_to and pointing_to != old_pointing_to:
			pointing_stack.push_back(pointing_to)
			_try_call(pointing_to, "pointer_enter")

	if pointing_to:
		var distance = $RayCast.get_collision_point().distance_to($RayCast.to_global(Vector3.ZERO))
		$RayMesh.mesh.height = distance
		$RayMesh.translation.z = -distance / 2

	self.active = self.get_pointing_to() != null
	$RayMesh.visible = self.active