extends ARVRController

var colorpicker
# The area that is being pointed to
var hover : Area = null
# Whether the trigger was pressed last tick
var trigger_last := false

func _ready() -> void:
	colorpicker = get_parent().get_node("LeftController/ColorPicker")
	colorpicker.connect("material_chosen", self, "_on_material_chosen")
	_on_material_chosen(colorpicker.current_material)
	colorpicker.connect("mode_set", self, "_on_color_picker_mode_set")

func play_rumble_anim(anim: String) -> void:
	$RumbleAnim.play(anim)

func _input(event: InputEvent) -> void:
	if event.device == get_joystick_id():
		if event is InputEventJoypadMotion:
			if not (event.axis in [0, 1]): return
			if event.axis_value == 0:
				$Pointer.joystick_release()
				return

			if colorpicker.mode:
				var pos := Vector2(get_joystick_axis(0), get_joystick_axis(1)).rotated(deg2rad(-120)) / Vector2(.5, .5)
				var val = (.5 - pos.y) / 1.5
				var sat = 0
				if val != 0:
					sat = (pos.x / 1.732) / val + .5
				sat = clamp(sat, 0, 1)
				val = clamp(val, 0, 1)
				colorpicker.set_sat_val(sat, val)
			else:
				$Pointer.joystick_motion(event)

func _process(delta: float) -> void:
	if is_button_pressed(15):
		if $Pointer.active:
			if !trigger_last:
				$Pointer.click()
			else:
				$Pointer.click_hold()
		else:
			if colorpicker.mode == colorpicker.Mode.ON:
				colorpicker.mode = colorpicker.Mode.OFF

		trigger_last = true
	else:
		trigger_last = false

func _on_color_picker_mode_set(mode: int) -> void:
	$Touchpad/Color.visible = (mode == colorpicker.Mode.ON)

func _on_material_chosen(mat: PaletteMaterial) -> void:
	if mat:
		var color = mat.get_spatial_material().albedo_color
		var inverted = color.inverted()
		$ExactCursor.mesh.material.albedo_color = inverted
		$Pointer/RayMesh.mesh.material.albedo_color = inverted

		$Touchpad/Color.material_override.set_shader_param("hue", colorpicker.hue)
		$Touchpad/Color/Indicator.material_override.albedo_color = color

		var indicator = Vector3(
			(color.s - .5) * color.v * 1.732,
			0,
			color.v * 1.5 - 1
		)
		$Touchpad/Color/Indicator.translation = indicator.rotated(Vector3.UP, deg2rad(120))