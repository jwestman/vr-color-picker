extends ARVROrigin

func _ready():
	var interface = ARVRServer.find_interface("OpenVR")
	if interface:
		if interface.initialize():
			# Tell our viewport it is the arvr viewport:
			get_viewport().arvr = true

			# turn off vsync, we'll be using the headsets vsync
			OS.vsync_enabled = false

			# change our physics fps
			Engine.target_fps = 90

			# make sure HDR rendering is off (not applicable for GLES2 renderer)
			get_viewport().hdr = false
		else:
			print("WARNING: Interface could not be initialized!")
	else:
		print("WARNING: Interface not found!")
