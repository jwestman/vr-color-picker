extends Node

const CONFIG_PATH := "user://global_palette.ini"
const PALETTE_SIZE := 4 * 8

var _config : ConfigFile

func _ready():
	_config = ConfigFile.new()
	if _config.load(CONFIG_PATH) != OK:
		_setup_default()

func _setup_default():
	_set_material(16, PaletteMaterial.new(Color("FF0000")))
	_set_material(17, PaletteMaterial.new(Color("FFA500")))
	_set_material(18, PaletteMaterial.new(Color("FFFF00")))
	_set_material(19, PaletteMaterial.new(Color("00FF00")))
	_set_material(20, PaletteMaterial.new(Color("0000FF")))
	_set_material(21, PaletteMaterial.new(Color("800080")))
	_set_material(22, PaletteMaterial.new(Color("964B00")))
	_set_material(23, PaletteMaterial.new(Color("FFFFFF")))

	_set_material(24, PaletteMaterial.new(Color("000000")))
	_set_material(25, PaletteMaterial.new(Color("202020")))
	_set_material(26, PaletteMaterial.new(Color("404040")))
	_set_material(27, PaletteMaterial.new(Color("606060")))
	_set_material(28, PaletteMaterial.new(Color("808080")))
	_set_material(29, PaletteMaterial.new(Color("A0A0A0")))
	_set_material(30, PaletteMaterial.new(Color("C0C0C0")))
	_set_material(31, PaletteMaterial.new(Color("E0E0E0")))

func set_material(pos: int, mat: PaletteMaterial) -> void:
	_set_material(pos, mat)
	_config.save(CONFIG_PATH)

func _set_material(pos: int, mat: PaletteMaterial) -> void:
	if pos >= PALETTE_SIZE: return
	if pos < 0: return

	_config.set_value("materials", str(pos), Utils.material_s(mat))

func get_material(pos: int) -> PaletteMaterial:
	var mat = _config.get_value("materials", str(pos), null)
	if mat:
		return Utils.material_d(mat)
	else:
		return null