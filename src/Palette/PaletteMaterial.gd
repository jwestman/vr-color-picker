extends Node
class_name PaletteMaterial

var color := Color(1, 0, 0, 1)
var _spatial_material : SpatialMaterial = null

func _init(color: Color) -> void:
	self.color = color

# Gets a new spatial material matching this
# PaletteMaterial
func get_spatial_material() -> SpatialMaterial:
	if self._spatial_material == null:
		var mat = SpatialMaterial.new()
		setup_spatial_material(mat)
		self._spatial_material = mat

	return self._spatial_material

# Sets up the given spatial material to match
# this PaletteMaterial
func setup_spatial_material(mat: SpatialMaterial) -> void:
	mat.albedo_color = self.color
	mat.flags_transparent = true
	mat.params_depth_draw_mode = SpatialMaterial.DEPTH_DRAW_ALWAYS
