extends Area

enum Mode { BUTTON, TOGGLE, GROUP }

export var is_toggled := false setget set_toggled
export(int, "Button", "Toggle", "Group") var mode := Mode.BUTTON

signal pressed
signal toggled

func set_toggled(new_toggled: bool) -> void:
	if is_toggled == new_toggled: return

	is_toggled = new_toggled
	if is_toggled:
		$Anim.play("Toggle")
	else:
		$Anim.play_backwards("Toggle")


func pointer_enter(pointer) -> void:
	$HoverAnim.play("Hover")

func pointer_exit(pointer) -> void:
	$HoverAnim.play_backwards("Hover")

func click(pointer) -> void:
	match mode:
		Mode.BUTTON:
			$Anim.play("Press")
			emit_signal("pressed")
		Mode.TOGGLE:
			if is_toggled:
				is_toggled = false
				$Anim.play_backwards("Toggle")
			else:
				is_toggled = true
				$Anim.play("Toggle")
			emit_signal("toggled")
		Mode.GROUP:
			pass
