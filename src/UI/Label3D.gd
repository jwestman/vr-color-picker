extends Spatial
tool

export var text := "" setget set_text

func set_text(new_text: String) -> void:
	text = new_text
	$Viewport/Label.text = text
	_update()

func _update() -> void:
	var size = $Viewport/Label.rect_size
	$Viewport.size = size
	$Surface.scale.x = size.x / 100
	$Surface.scale.y = size.y / 100
