extends Spatial
tool

export var color : Color setget set_color
export var length := .5 setget set_length

export var min_val := 0.0 setget set_min_val
export var max_val := 1.0  setget set_max_val
export var step := 1.0 / 255.0  setget set_step
export var value : float = 0.0  setget set_value

signal value_changed


var is_ready := false

func _ready() -> void:
	is_ready = true
	_recalc()

	var mat = SpatialMaterial.new()
	mat.flags_unshaded = true
	mat.albedo_color = color
	$Bar.material_override = mat

func click_hold(pointer) -> void:
	var val = (pointer.get_pointer_location().x / length) + .5
	set_value(val * (max_val - min_val) + min_val)
	emit_signal("value_changed", value)

func pointer_enter(pointer) -> void:
	$anim.play("Hover")

func pointer_exit(pointer) -> void:
	$anim.play_backwards("Hover")

func swipe_left(pointer) -> void:
	set_value(value - step)
	emit_signal("value_changed", value)

func swipe_right(pointer) -> void:
	set_value(value + step)
	emit_signal("value_changed", value)

func get_swipe_step(pointer) -> float:
	return 1.0 / 8

func _recalc() -> void:
	if not is_ready: return

	$Bar.scale.x = length
	$Collision.scale.x = length

	var normal := (value - min_val) / (max_val - min_val)
	$Knob.translation.x = (normal - 0.5) * length


func set_color(value: Color) -> void:
	color = value
	_recalc()

func set_length(value: float) -> void:
	length = value
	_recalc()

func set_min_val(value: float) -> void:
	if value < max_val:
		min_val = value
		_recalc()

func set_max_val(value: float) -> void:
	if value > min_val:
		max_val = value
		_recalc()

func set_step(value: float) -> void:
	step = value
	_recalc()

func set_value(new_value: float) -> void:
	value = stepify(clamp(new_value, min_val, max_val), step)
	_recalc()
