extends Object
class_name Utils

const DEAD_ZONE = .1

static func hue_to_angle(hue: float) -> float:
	return hue * deg2rad(360)

static func material_s(mat: PaletteMaterial) -> String:
	return to_json({
		"color": mat.color.to_html()
	})

static func material_d(string: String) -> PaletteMaterial:
	var dict = parse_json(string)
	return PaletteMaterial.new(Color(dict.color))